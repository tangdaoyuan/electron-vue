import Vue from 'vue'
import VueRouter from 'vue-router'
import { Route } from 'vue-router'
import { CONSTANT } from '../src/renderer/constants/beans'

declare module 'vue/types/vue' {
  interface Vue {
    $router: VueRouter,
    $route: Route,
    _: any,
    $: any,
    $http: any,
    Cookies: any,
    CONSTANT: CONSTANT
  }
}
