import Vue from 'vue'
import axios from 'axios'
import * as ElementUI from 'element-ui'
import * as vueElectron from 'vue-electron'
import Cookies from 'js-cookie'
import App from './App.vue'
import router from './routers'
import store from './store'
import CONSTANT from './constants/beans'
import 'element-ui/lib/theme-chalk/index.css'
import './assets/sass/main.scss'

if (!process.env.IS_WEB) Vue.use(vueElectron)

Vue.prototype.$http = axios
Vue.config.productionTip = false
Vue.use(ElementUI)

Vue.prototype = Object.assign(Vue.prototype, {
  Cookies,
  CONSTANT
})

/* eslint-disable no-new */
new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount('#app')
