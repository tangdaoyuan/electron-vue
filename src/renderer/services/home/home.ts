import Service from '..'

interface HomeBase<T> {
  home (data: object): Promise<T>
}

class HomeService extends Service implements HomeBase<any> {
  constructor (path: string = '/api/home') {
    super(path)
  }
  public home (data: object): Promise<any> {
    return new Promise((resolve: any) => {
      super.get('/home', data, resolve)
    })
  }
}

export default HomeService
