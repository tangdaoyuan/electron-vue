import HomeService from './home'
export class IndexService {
  private _homeService !: HomeService

  public get homeService (): HomeService {
    if (!this._homeService) this._homeService = new HomeService()
    return this._homeService
  }
}
export default new IndexService()
